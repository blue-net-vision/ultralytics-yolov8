# Ultralytics YOLOv8

#### 介绍
快速上手YOLOv8并在streamlit上实践模型

[官方快速学习通道](https://docs.ultralytics.com/modes/)

* 因为上面链接讲的非常完备，足够让你quick start

# 数据收集-->预模型加载-->模型训练-->模型预测 全套大礼包
## 1.conda创建自己的虚拟环境
安装库

    pip install ultralytics -i https://pypi.tuna.tsinghua.edu.cn/simple
    pip install roboflow -i https://pypi.tuna.tsinghua.edu.cn/simple

## 2.图片数据下载 

[这里搜索你想识别的任何图片数据集(同时也包括分割，分类等)](https://universe.roboflow.com/)

tips:这些数据集是开箱即用，都是已经做好标签和分好类的了

### 下载流程
点击download

![点击download](https://gitee.com/blue-net-vision/ultralytics-yolov8/raw/master/pic/2.png)

点击continue

![点击continue](https://gitee.com/blue-net-vision/ultralytics-yolov8/raw/master/pic/1.png) 

点击复制然后粘贴到你的代码单元格上

![点击复制](https://gitee.com/blue-net-vision/ultralytics-yolov8/raw/master/pic/3.png)

## 3.官方做法是在Colab(云主机，有限时的GPU资源)上实现，这对只有cpu电脑的人较友好，因为cpu根本跑不动
本地实现(使用ipynb)：

导入库

    from ultralytics import YOLO
    import os
    from IPython.display import display, Image
    from IPython import display
    display.clear_output()
    !yolo mode=checks

然后粘贴上面的图片数据的下载代码，运行后就会将数据下载到当前目录，点击目录，修改data.yaml，将里面三个路径改为train,test,val的绝对路径(可以不改路径先训练，看看是否报错)

然后就是CLI式一段代码训练：

    !yolo task=detect mode=train model=yolov8m.pt data={dataset.location}/data.yaml epochs=20 imgsz=640

这里训练完呢会在你的weights文件夹中生成一个best.pt,就是你训练得到的模型。

## 4.val & predict(可以跳过)
    !yolo task=detect mode=val model=/path/to/pt data={dataset.location}/data.yaml

    !yolo task=detect mode=predict model=/path/to/pt conf=0.5 data={dataset.location}/data.yaml


## 5.实例

    !yolo task=detect mode=predict model=/content/runs/detect/train/weights/best.pt conf=0.5 source='path/to/your_pic_or_video'

打开外接摄像头进行实时识别，save是保存视频流，show是有视频流输出，跟cv2.imshow()一样

    !yolo task=detect mode=predict model=RGB.pt conf=0.5 source=1 show=True save=True



